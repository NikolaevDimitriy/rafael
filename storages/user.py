__author__ = 'dimitriy'

from models.user import User
from domain_core.storages import PgBaseReadingStorage, PgBaseWritingStorage


class UserStorage(PgBaseReadingStorage, PgBaseWritingStorage):
    model = User
    get_func = 'SELECT * FROM public.user_get($1::INTEGER);'
    get_list_func = 'SELECT * FROM public.users_get($1::INTEGER, $2::INTEGER, $3::TEXT, $4::TEXT);'

    save_func = 'SELECT public.user_save();'

