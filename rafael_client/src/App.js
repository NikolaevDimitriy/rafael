import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import { BrowserRouter } from 'react-router-dom';
import RafaelApp from './components/RafaelApp'

const store = configureStore()

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <RafaelApp/>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
