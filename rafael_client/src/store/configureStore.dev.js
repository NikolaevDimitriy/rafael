import { createStore, applyMiddleware, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import reducers from '../reducers'


export default function configureStore() {

  const store = createStore(
    combineReducers({ ...reducers}),
    composeWithDevTools(applyMiddleware(thunk))
    )

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const newReducers = require('../reducers').default // eslint-disable-line
      store.replaceReducer(combineReducers({ ...newReducers }))
    })
  }

  return store
}
