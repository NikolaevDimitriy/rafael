import { createStore, compose, applyMiddleware, combineReducers } from 'redux'
import thunkMiddleware from 'redux-thunk'
import reducers from '../reducers/index'

export default function configureStore() {
  const initialState = {}

  const store = createStore(
    combineReducers({ ...reducers}),
    initialState,
    compose(applyMiddleware(thunkMiddleware))
  )

  return store
}
