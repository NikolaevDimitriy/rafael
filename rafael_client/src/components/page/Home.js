import React, { Component } from 'react'
import HomeSlider from '../partial/slider/HomeSlider'


export default class Home extends Component {
  render() {
    return (
      <div className="home">
        <HomeSlider />

      </div>
    );
  }
}