import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Slider from 'react-slick'
import './HomeSlider.css'


export default class HomeSlider extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
    <div>
      <Slider {...settings}>
        <div><img src="./static/img/slide1.jpg"/></div>
        <div><h3>2</h3></div>
        <div><h3>3</h3></div>
        </Slider>
     </div>
    );
  }
}