import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './Menu.css'


export default class Menu extends Component {
  render() {
    return (
      <nav className="menu">
        <ul>
            <li><Link to='/catalogue'>Catalogue</Link></li>
            <li><Link to='/delivery_payment'>Delivery and payment</Link></li>
            <li><Link to='/about'>About</Link></li>
        </ul>
      </nav>
    );
  }
}