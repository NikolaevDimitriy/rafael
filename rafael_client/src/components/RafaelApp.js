import React, { Component } from 'react'
import Header from './Header'
import Footer from './Footer'
import Wrapper from './Wrapper'

export default class RafaelApp extends Component {
  render() {
    return (
      <div className="app">
        <Header/>
        <Wrapper/>
        <Footer/>
      </div>
    );
  }
}

