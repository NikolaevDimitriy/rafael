import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './page/Home'
import Catalogue from './page/Catalogue'
import Product from './page/Product'
import DeliveryPayment from './page/DeliveryPayment'
import About from './page/About'


export default class Wrapper extends Component {
  render() {
    return (
      <section className="wrapper">
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/catalogue" component={Catalogue}/>
          <Route path="/product" component={Product}/>
          <Route path="/delivery_payment" component={DeliveryPayment}/>
          <Route path="/about" component={About}/>
        </Switch>
      </section>
    );
  }
}