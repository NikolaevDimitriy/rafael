import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Menu from './partial/Menu'
import Basket from './partial/basket/Basket'
import logo from '../logo.png'
import './Header.css'


export default class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="region">Ваш регион доставки: Москва</div>
        <div className="phone">8 800 000 00 00</div>
        <Link className="logo" to="/"><img src={logo}/></Link>
        <Basket/>
        <Menu/>
      </header>
    );
  }
}