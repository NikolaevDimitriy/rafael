__author__ = 'dimitriy'

import datetime
from enum import IntEnum
from typing import Any
from domain_core.models import DeletableModel


class User(DeletableModel):

    class Statuses(IntEnum):
        Undefined = 0
        Active = 1
        Inactive = 2
        Blocked = 3

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        self.status = kwargs.get('status', User.Statuses.Inactive)
        self.is_admin = kwargs.get('is_admin', False)
        self.name = kwargs.get('name')
        self.surname = kwargs.get('surname')
        self.email = kwargs.get('email')
        self.phone = kwargs.get('phone')
        self.pwd = kwargs.get('pwd')
        self.avatar = kwargs.get('avatar')
        self.creation_date = kwargs.get('creation_date', datetime.datetime.utcnow())

    def from_flat_dict(self, **kwargs) -> Any:
        super(User, self).from_flat_dict(**kwargs)
        return self

    def to_flat_dict(self, **kwargs) -> dict:
        res = super(User, self).to_flat_dict(**kwargs)
        return res

    def from_dict(self, **kwargs) -> Any:
        super(User, self).from_dict(**kwargs)
        return self

    def to_dict(self, **kwargs) -> dict:
        res = super(User, self).to_dict()
        return res

    def set_simple_fields(self, **kwargs):
        self.status = kwargs.get('status')
        self.is_admin = kwargs.get('is_admin')
        self.name = kwargs.get('name')
        self.surname = kwargs.get('surname')
        self.email = kwargs.get('email')
        self.phone = kwargs.get('phone')
        self.pwd = kwargs.get('pwd')
        self.avatar = kwargs.get('avatar')
        self.creation_date = kwargs.get('creation_date')

    def simple_fields_to_dict(self, res):
        res['status'] = self.status
        res['is_admin'] = self.is_admin
        res['name'] = self.name
        res['surname'] = self.surname
        res['email'] = self.email
        res['phone'] = self.phone
        res['pwd'] = self.pwd
        res['avatar'] = self.avatar
        res['creation_date'] = self.creation_date
