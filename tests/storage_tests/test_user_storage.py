__author__ = 'dimitriy'
import datetime
import pytest
from domain_core.providers import ProviderTypes, create_data_providers
from storages.user import UserStorage
from models.user import User


@pytest.mark.asyncio
async def test_user_get(event_loop, db_config):
    providers = await create_data_providers(ProviderTypes.PG, db_config.get('db'), event_loop)

    storage = UserStorage(providers)

    user = await storage.get(1)

    assert user.email == 'admin@rafael.mail'
    assert user.phone == '89052735699'
    assert user.name == 'admin'
    assert user.surname == 'adminov'
    assert user.is_admin
    assert not user.deleted
    assert user.status == User.Statuses.Active
    assert user.pwd == b''
    assert user.avatar == '/static/img/avatar.png'


@pytest.mark.asyncio
async def test_user_save(event_loop, db_config):
    providers = await create_data_providers(ProviderTypes.PG, db_config.get('db'), event_loop)

    storage = UserStorage(providers)
    date = datetime.datetime.utcnow()
    email = '%d%d%d%d%d%d%dmail@rafael.mail' % (date.year, date.month, date.day, date.hour, date.minute, date.second, date.microsecond,)
    phone = '+7%d%d%d%d%d%d%d' % (date.year, date.month, date.day, date.hour, date.minute, date.second, date.microsecond,)
    user = User(status=User.Statuses.Active,
                name='test_code',
                surname='test_codov',
                email=email,
                phone=phone,
                pwd=b'',
                avatar='/static/img/avatar.png')

    user.id = await storage.save(user)


