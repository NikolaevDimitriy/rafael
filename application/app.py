__author__ = 'dimitriy'
import os
import argparse
import asyncio
import uvloop
from aiohttp import web
from settings import build_config
import aiohttp_debugtoolbar
import logging
from web_core.application import init_urls
from application.urls import Rafael
# from domain_core.providers import create_data_providers

async def handler(request):
    return web.Response(text='ok')


async def init_app(loop=None, config=None):
    # dp = await create_data_providers(loop)
    app = web.Application(loop=loop, debug=config.get('debug', False))

    if app.debug:
        logging.basicConfig(filename='app.log', level=app.debug)

    # install aiohttp_debugtoolbar
    if app.debug:
        aiohttp_debugtoolbar.setup(app, intercept_redirects=False)

    # app.data_providers = dp
    init_urls(app.router, Rafael())
    app.router.add_route("*", '/test', handler)
    app.on_cleanup.append(shutdown)
    return app


async def shutdown(app):
    if hasattr(app, 'data_providers'):
        app.data_providers.close()
        await app.data_providers.wait_closed()
        await app.amqp_protocol.close()
        app.amqp_transport.close()


#  set uvloop
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
#  get declare port arg
parser = argparse.ArgumentParser(description="rafael server")
parser.add_argument('--port', type=int, default=8080)
parser.add_argument('--cfg', type=str)

if __name__ == '__main__':

    print(os.getcwd())
    args = parser.parse_args()
    loop = asyncio.get_event_loop()
    f = init_app(loop=loop, config=build_config(args.cfg))
    srv = loop.run_until_complete(f)
    #  run app
    web.run_app(srv, host='127.0.0.1', port=args.port)