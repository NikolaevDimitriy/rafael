/* pgmigrate-encoding: utf-8 */
DO $$ begin raise notice 'initial tests'; end; $$;


CREATE OR REPLACE FUNCTION user_crud_test()
    RETURNS VOID
    LANGUAGE 'plpgsql'
    VOLATILE
AS $function$
DECLARE user_id INTEGER;
DECLARE user_instance RECORD;

BEGIN
  SELECT user_save(
    0::INTEGER,
    1::SMALLINT,
    FALSE::BOOLEAN,
    'test@rafael.mail'::TEXT,
    '+79612431189'::TEXT,
    ''::BYTEA,
    'test'::TEXT,
    'testov'::TEXT,
    now() AT TIME ZONE 'utc',
    '/static/img/avatar.png'::TEXT
  ) INTO user_id;

  ASSERT user_id > 0;

  PERFORM user_del(user_id);

  SELECT * INTO user_instance FROM public.user_get(user_id);

  ASSERT user_instance.id = user_id;
  ASSERT user_instance.is_admin = FALSE;
  ASSERT user_instance.status = 1;
  ASSERT user_instance.email = 'test@rafael.mail';
  ASSERT user_instance.phone = '+79612431189';
  ASSERT user_instance.deleted = TRUE;
  ASSERT user_instance.del_time IS NOT NULL;
  ASSERT user_instance.name = 'test';
  ASSERT user_instance.surname = 'testov';
  ASSERT user_instance.avatar = '/static/img/avatar.png';

  DELETE FROM public.users WHERE id = user_id;

END;
$function$;

CREATE OR REPLACE FUNCTION create_test_env()
    RETURNS VOID
    LANGUAGE 'plpgsql'
    VOLATILE
AS $function$
DECLARE user_id INTEGER;

BEGIN
  SELECT user_save(
    0::INTEGER,
    1::SMALLINT,
    TRUE::BOOLEAN,
    'admin@rafael.mail'::TEXT,
    '89052735699'::TEXT,
    ''::BYTEA,
    'admin'::TEXT,
    'adminov'::TEXT,
    now() AT TIME ZONE 'utc',
    '/static/img/avatar.png'::TEXT
  ) INTO user_id;

  ASSERT user_id > 0;

  SELECT user_save(
    0::INTEGER,
    1::SMALLINT,
    FALSE::BOOLEAN,
    'user@rafael.mail'::TEXT,
    '+79052735699'::TEXT,
    ''::BYTEA,
    'user'::TEXT,
    'userov'::TEXT,
    now() AT TIME ZONE 'utc',
    '/static/img/avatar.png'::TEXT
  ) INTO user_id;

  ASSERT user_id > 0;
END;
$function$;


CREATE OR REPLACE FUNCTION exec_tests()
    RETURNS VOID
    LANGUAGE 'plpgsql'
    VOLATILE
AS $function$
BEGIN
  perform user_crud_test();
END;
$function$;