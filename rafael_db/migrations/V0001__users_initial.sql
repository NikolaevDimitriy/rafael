/* pgmigrate-encoding: utf-8 */
DO $$ begin raise notice 'initial migration'; end; $$;


CREATE TABLE public.users (
  id serial PRIMARY KEY,
  status SMALLINT NOT NULL,
  is_admin BOOLEAN NOT NULL DEFAULT FALSE,
  deleted boolean NOT NULL DEFAULT FALSE,
  creation_date TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  email TEXT UNIQUE NOT NULL,
  phone TEXT UNIQUE NOT NULL,
  name TEXT NOT NULL,
  surname TEXT NOT NULL,
  pwd BYTEA NOT NULL,
  avatar TEXT,
  del_time TIMESTAMP WITHOUT TIME ZONE
)
WITH(
  OIDS = FALSE
);

ALTER TABLE public.users
  OWNER TO rafael;

--user_save-------------------------------------------------------------------------------------------------------------
CREATE FUNCTION public.user_save(
identity INTEGER,
status SMALLINT,
is_admin BOOLEAN,
email TEXT,
phone TEXT,
pwd BYTEA,
name TEXT,
surname TEXT,
creation_date TIMESTAMP WITHOUT TIME ZONE,
avatar TEXT)
RETURNS INTEGER AS
$BODY$
DECLARE res_id INTEGER;
BEGIN
  IF identity > 0 THEN
      UPDATE users SET
        status = status, is_admin = is_admin,
        email = email, phone = phone, pwd = pwd,
        name = name, surname = surname,
        avatar = avatar
      WHERE id = identity;
      res_id := identity;
  ELSE
    INSERT INTO users (status, is_admin, email, phone, pwd, name, surname, creation_date, avatar)
    VALUES (status, is_admin, email, phone, pwd, name, surname, creation_date, avatar) returning id INTO res_id;
  END IF;
  RETURN res_id;
END;
$BODY$
LANGUAGE plpgsql;

ALTER FUNCTION public.user_save(
    INTEGER, SMALLINT, BOOLEAN, TEXT, TEXT, BYTEA, TEXT, TEXT, TIMESTAMP WITHOUT TIME ZONE, TEXT)
  OWNER TO rafael;

--user_get--------------------------------------------------------------------------------------------------------------
CREATE FUNCTION public.user_get(identity INTEGER)
RETURNS TABLE (
  id INTEGER,
  status SMALLINT,
  is_admin BOOLEAN,
  deleted BOOLEAN,
  creation_date TIMESTAMP WITHOUT TIME ZONE,
  email TEXT,
  phone TEXT,
  pwd BYTEA,
  name TEXT,
  surname TEXT,
  avatar TEXT,
  del_time TIMESTAMP WITHOUT TIME ZONE
) AS $BODY$
BEGIN
  RETURN QUERY
  SELECT u.id, u.status, u.is_admin, u.deleted, u.creation_date, u.email, u.phone, u.pwd, u.name, u.surname, u.avatar, u.del_time
  FROM public.users u
  WHERE u.id = identity;
END;
$BODY$
LANGUAGE plpgsql;

ALTER FUNCTION public.user_get(INTEGER)
  OWNER TO rafael;

--user_del--------------------------------------------------------------------------------------------------------------
CREATE FUNCTION public.user_del(identity INTEGER)
RETURNS VOID AS
$BODY$
BEGIN
  UPDATE users SET del_time = now() AT TIME ZONE 'utc', deleted = TRUE WHERE id = identity;
END;
$BODY$ LANGUAGE plpgsql;

ALTER FUNCTION public.user_del(INTEGER)
  OWNER TO rafael;

--users_get-------------------------------------------------------------------------------------------------------------
CREATE FUNCTION public.users_get(page_limit INTEGER, page_offset INTEGER, sort_col TEXT, sort_dir TEXT)
RETURNS TABLE(
  id INTEGER,
  status SMALLINT,
  is_admin BOOLEAN,
  deleted BOOLEAN,
  creation_date TIMESTAMP WITHOUT TIME ZONE,
  email TEXT,
  phone TEXT,
  pwd BYTEA,
  name TEXT,
  surname TEXT,
  avatar TEXT,
  del_time TIMESTAMP WITHOUT TIME ZONE
) AS
$BODY$
BEGIN
  RETURN QUERY
  SELECT
    u.id, u.status, u.is_admin, u.deleted, u.creation_date,
    u.email, u.phone, u.pwd, u.name, u.surname, u.avatar, u.del_time
  FROM public.users u
  WHERE not u.deleted
  ORDER BY
    CASE sort_col || sort_dir WHEN 'emailASC' THEN u.email END ASC,
    CASE sort_col || sort_dir WHEN 'emailDESC' THEN u.email END DESC,

    CASE sort_col || sort_dir WHEN 'creation_dateASC' THEN u.creation_date END ASC,
    CASE sort_col || sort_dir WHEN 'creation_dateDESC' THEN u.creation_date END DESC,

    CASE sort_col || sort_dir WHEN 'statusASC' THEN u.status END ASC,
    CASE sort_col || sort_dir WHEN 'statusDESC' THEN u.status END DESC;

END;
$BODY$ LANGUAGE plpgsql;

ALTER FUNCTION public.users_get(INTEGER, INTEGER, TEXT, TEXT)
  OWNER TO rafael;

INSERT INTO ops (op) VALUES ('migration V0001__users_initial.sql');
